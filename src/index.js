import { render } from 'inferno';
import 'inferno-devtools';
import watchlist from './utils/watchlist';
import Root from './components/Root';
import store from './redux/store';
import './css/index.css';

watchlist.setup();

render(
  <Root store={store} />,
  document.getElementById('root'),
);
