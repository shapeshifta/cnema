import { createStore, applyMiddleware } from 'redux';
import ApiService from '../utils/apiservice';
import { logger, crashReporter } from './middleware';

const store = createStore((state, action) => {
  switch (action.type) {
    case 'GET_MOVIE_LIST':
      ApiService
        .getMovieList(action.payload.search)
        .then((data) => {
          if (data.results) {
            store.movies = data.results;
            store.lastSearch = action.payload.search;
          } else {
            store.movies = null;
          }
          store.dispatch({
            type: 'GET_MOVIE_LIST_SUCCESS',
          });
        });
      return {
        name: action.name,
      };
    case 'GET_MOVIE_LIST_SUCCESS':
      return {
        name: action.name,
        movies: store.movies,
        lastSearch: store.lastSearch,
      };
    case 'GET_MOVIE':
      ApiService
        .getMovie(action.payload.id)
        .then((data) => {
          if (data) {
            store.currentMovie = data;
          } else {
            store.currentMovie = null;
          }
          store.dispatch({
            type: 'GET_MOVIE_SUCCESS',
          });
        });
      return {
        name: action.name,
      };
    case 'GET_MOVIE_SUCCESS':
      return {
        name: action.name,
        currentMovie: store.currentMovie,
      };
    default:
      return {
        name: 'DEFAULT',
      };
  }
}, applyMiddleware(logger, crashReporter));

export default store;
