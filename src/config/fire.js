import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: process.env.INFERNO_APP_FIREBASE_API_KEY,
  authDomain: `${process.env.INFERNO_APP_FIREBASE_PROJECT_ID}.firebaseapp.com`,
  databaseURL: `https://${process.env.INFERNO_APP_FIREBASE_PROJECT_ID}.firebaseio.com`,
  projectId: process.env.INFERNO_APP_FIREBASE_PROJECT_ID,
  storageBucket: `${process.env.INFERNO_APP_FIREBASE_PROJECT_ID}.appspot.com`,
  messagingSenderId: '391832060509',
};

const fire = firebase.initializeApp(config);
export default fire;
