import Component from 'inferno-component';
import fire from '../config/fire';

class Logout extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout(e) {
    e.preventDefault();
    fire.auth().signOut().then(() => {
      // Sign-out successful.
      console.log('signed out');

      this.setState({
        email: '',
        password: '',
      });
    }, error => {
      // An error happened.
    });
  }

  render() {
    return (
      <form>
        <button type="submit" onClick={this.logout} className="button">Logout</button>
      </form>
    );
  }
}
export default Logout;
