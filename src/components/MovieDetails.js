import Component from 'inferno-component';
import { Link } from 'inferno-router';
import watchlist from '../utils/watchlist';

class Movie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMovie: undefined,
    };
  }

  componentDidMount() {
    const { props } = this;
    const { store } = this.context;

    // subscribe to store
    store.subscribe(() => {
      if (store.currentMovie) {
        this.setState({
          currentMovie: store.currentMovie,
        }, () => {
        });
      }
    });

    // dispatch movie update
    store.dispatch({
      type: 'GET_MOVIE',
      payload: {
        id: props.params.id,
      },
    });
  }

  handleButtonClick(command = null, id = null) {
    if (command && id) {
      watchlist[command](id);
      this.setState(this.state);
    }
  }

  static getImage(img = null) {
    let path = '';
    if (!img) {
      path = 'http://via.placeholder.com/500x550?text=no+image';
    } else {
      path = `https://image.tmdb.org/t/p/w500/${img}`;
    }
    return <img src={path} alt="movie poster" className='thumbnail'/>;
  }

  render() {
    const { currentMovie: movie } = this.state;

    if (!movie) {
      return 'Loading.';
    }

    return (
      <div>
        <div className="card">
          <div className="card-divider">
            <div className="button-group float-right" style={{ 'margin-bottom': 0 }}>
              <Link to={'/'} className='button'>
                <i className="fi-magnifying-glass"></i>
              </Link>
              <Link to={'/watchlist'}
                    className={watchlist.storedMovies.length ? 'success button float-right tiny' : 'hide'}>
                <i className="fi-list-thumbnails"></i>
              </Link>
            </div>
            <h2>{movie.title}</h2>
            <span className="secondary label">Released: {movie.release_date}</span>&nbsp;
            <span className="primary label">Rating: {movie.vote_average} ({movie.vote_count} votes)</span>&nbsp;
          </div>
          <div className="card-section">
            <div className="row">
              <div className="small-12 medium-6 large-4 columns">
                {this.constructor.getImage(movie.poster_path)}
              </div>
              <div className="small-12 medium-6 large-8 columns">
                <p>{movie.overview}</p>
                <div className="row">
                  <div className="button-group tiny large-6 columns">
                    <a href={`http://www.imdb.com/title/${movie.imdb_id}`} target='_blank' className="button">Show on
                      imdb</a>
                    <a href={`https://www.rottentomatoes.com/search/?search=${movie.title}`} target='_blank'
                       className="button">Search on
                      rot</a>
                  </div>
                  <div className="button-group large-6 columns">
                    <button type="button"
                            className={!watchlist.contains(movie.id) ? 'success button float-right' : 'hide'}
                            onClick={this.handleButtonClick.bind(this, 'add', movie)}>
                      <i className="fi-heart"></i> Add to watchlist
                    </button>
                    <button type="button"
                            className={watchlist.contains(movie.id) ? 'alert button float-right' : 'hide'}
                            onClick={this.handleButtonClick.bind(this, 'remove', movie.id)}>
                      <i className="fi-x"></i> Remove from watchlist
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className={movie.videos.results.length ? 'row' : 'hide'} style={{ 'margin-top': '20px' }}>
              <div className="columns">
                {
                  movie.videos.results.map(video => <div className="responsive-embed">
                    <iframe width="420" height="315" title={video.key} src={`https://www.youtube.com/embed/${video.key}`} allowFullScreen>
                    </iframe>
                  </div>)
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Movie;
