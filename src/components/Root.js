import { Provider } from 'inferno-redux';
import { Router, Route } from 'inferno-router';
import createHistory from 'history/createBrowserHistory';
import MovieDetails from './MovieDetails';
import App from '../App';
import '../css/index.css';
import MovieList from './MovieList';

function NoMatch() {
  return <div>Sorry, page not found.</div>;
}

const Root = ({ store }) => (
  <Provider store={store}>
    <Router history={ createHistory() }>
      <Route path="/" component={ App }/>
      <Route path="/watchlist" component={ MovieList } showWatchlist={true}/>
      <Route path="/movie/:id" component={ MovieDetails }/>
      <Route path="*" component={ NoMatch }/>
    </Router>
  </Provider>
);

export default Root;
