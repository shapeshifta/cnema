import Component from 'inferno-component';

class Footer extends Component {
  render() {
    return (
      <div className="copy text-center">
        Icon made by <a href="http://www.flaticon.com/authors/dimi-kazak" title="Dimi Kazak">Dimi Kazak</a> from <a
        href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a
        href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0
        BY</a>
        <br/>
        Movie information provided by <a href="https://www.themoviedb.org/">TMDB</a>
      </div>
    );
  }
}

export default Footer;
