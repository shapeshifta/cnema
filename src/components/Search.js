import Component from 'inferno-component';

class Search extends Component {
  render() {
    const { store } = this.context;
    const handleChange = (e) => {
      e.preventDefault();
      store.dispatch({
        type: 'GET_MOVIE_LIST',
        payload: {
          search: e.target.value,
        },
      });
    };

    return (
      <div className="row search">
        <div className="medium-6 large-6 columns small-centered">
          <input placeholder="Please enter movie title" type="text" autoFocus
                 onKeyUp={ handleChange } value={ store.lastSearch } />
        </div>
      </div>
    );
  }
}

export default Search;
