import Component from 'inferno-component';
import { Link } from 'inferno-router';
import watchlist from '../utils/watchlist';
import Logo from '../logo';
import Logout from './Logout';

watchlist.setup();

class Header extends Component {
  render() {
    return (
      <div className="top-bar text-center">
        <Link to={'/watchlist'} className={watchlist.storedMovies.length ? 'success button float-right' : 'hide'} style={{ 'margin-left': '-100%' }}>
          <i className="fi-list-thumbnails"></i>
        </Link>
        { this.props.user ? (<Logout/>) : '' }
        <Logo width="80" height="80" className={'float-center'}/>
        <h2>{'Welcome to cnema'}</h2>
      </div>
    );
  }
}

export default Header;
