import Component from 'inferno-component';
import { Link } from 'inferno-router';
import '../css/MovieList.css';
import store from '../redux/store';
import watchlist from '../utils/watchlist';

class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      isHidden: true,
      lastSearch: null,
    };
  }

  componentDidMount() {
    if (!this.props.showWatchlist) {
      store.subscribe(() => {
        this.setState({
          movies: store.movies || [],
          isHidden: !this.state.movies.length,
          lastSearch: store.lastSearch,
        });
      });

      if (store.lastSearch) {
        store.dispatch({
          type: 'GET_MOVIE_LIST',
          payload: {
            search: store.lastSearch,
          },
        });
      }
    } else {
      this.setState({
        movies: watchlist.storedMovies || [],
        isHidden: !watchlist.storedMovies.length,
        lastSearch: store.lastSearch,
      });
    }
  }

  handleButtonClick() {
    watchlist.empty();
    this.setState({
      movies: [],
      isHidden: true,
      lastSearch: store.lastSearch,
    });
  }

  render() {
    const { movies } = this.state;
    const { showWatchlist } = this.props;

    function getImage(img = null) {
      let path = '';
      if (!img) {
        path = 'http://via.placeholder.com/500x550/ffffff/000000?text=no+image';
      } else {
        path = `https://image.tmdb.org/t/p/w500/${img}`;
      }
      return <img src={path} alt="movie poster" className='thumbnail float-center'/>;
    }

    return (
      <div>
        <div className={showWatchlist ? 'row' : 'hide'}>
          <div className="button-group medium float-right">
            <Link to={'/'} className='button'>
              <i className="fi-magnifying-glass"></i>
            </Link>
            <button className={watchlist.storedMovies.length ? 'button alert' : 'hide'} onClick={this.handleButtonClick.bind(this)}>
              Remove all
            </button>
          </div>
        </div>
        <ul id="results"
            className={this.state.isHidden ? 'hide' : 'results row inline-list'}>
          {
            movies.map(movie =>
              <li className="columns small-up-1 medium-up-3 large-up-5">
                <Link to={`/movie/${movie.id}`}>
                  <div className="card">
                    <div className="card-divider">{movie.title}</div>
                    {getImage(movie.poster_path)}
                    <div className="card-section">
                      <span className="secondary label">Released: {movie.release_date}</span>
                      <span className="primary label">Rating: {movie.vote_average}</span>
                    </div>
                  </div>
                </Link>
              </li>)
          }
        </ul>
  </div>
    );
  }
}

export default MovieList;
