import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const USER_ID = localStorage.getItem('user');
const USER_PATH = `user/${USER_ID}`;

const watchlist = {
  storedMovies: [],
  setup() {
    firebase.database().ref(USER_PATH).on('value', (snapshot) => {
      watchlist.storedMovies = snapshot.val() ? snapshot.val().movies : [];
    }, (error) => {
      console.log(`Error: ${error.code}`);
      watchlist.storedMovies = [];
    });
  },
  add(movie = null) {
    if (watchlist.storedMovies && movie) {
      if (watchlist.storedMovies.includes(movie)) {
        return;
      }

      watchlist.storedMovies.push(movie);
      firebase.database().ref(USER_PATH).set({
        movies: watchlist.storedMovies || [],
      });
    }
  },
  remove(id = null) {
    if (watchlist.storedMovies && id) {
      const movieToRemove = watchlist.storedMovies.find(movie => movie.id === id);

      watchlist.storedMovies = watchlist.storedMovies.filter(movie => movie !== movieToRemove);
      firebase.database().ref(USER_PATH).set({
        movies: watchlist.storedMovies,
      });
    }
  },
  empty() {
    firebase.database().ref(USER_PATH).set({
      movies: [],
    });
  },
  contains(id = null) {
    return watchlist.storedMovies.find(movie => movie.id === id);
  },
};

export default watchlist;
