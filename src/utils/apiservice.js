// src/utils/ApiService.js
// @TODO: find better way to build api request
function apiRequest(path = '', search = '/search') {
  const url = `https://api.themoviedb.org/3${search}/movie${path}?`;
  const key = `api_key=${process.env.INFERNO_APP_TMDB_KEY}`;
  const lang = '&language=en-EN';
  return `${url + key + lang}&page=1&include_adult=true&append_to_response=videos,images`;
}

// Handle fetch errors
function handleError(error) {
  console.error('An error occurred:', error);
  throw error;
}

// Verify that the fetched response is JSON
function verifyResponse(res) {
  const contentType = res.headers.get('content-type');
  if (contentType && contentType.indexOf('application/json') !== -1) {
    return res.json();
  }
  handleError({ message: 'Response was not JSON' });
  return false;
}

// GET list of all movies from API
function getMovieList(movie) {
  return fetch(`${apiRequest()}&query=${movie}`)
    .then(verifyResponse, handleError);
}

// GET a movie detail info from API by title
function getMovie(movie) {
  return fetch(apiRequest(`/${movie}`, ''))
    .then(verifyResponse, handleError);
}

// Export ApiServices
export default {
  getMovieList,
  getMovie,
};
