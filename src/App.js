import Component from 'inferno-component';
import Header from './components/Header';
import Footer from './components/Footer';
import Search from './components/Search';
import MovieList from './components/MovieList';
import Login from './components/Login';
import fire from './config/fire';
import './css/App.css';

class App extends Component {
  constructor() {
    super();
    this.state = ({
      user: null,
    });
    this.authListener = this.authListener.bind(this);
  }

  componentDidMount() {
    this.authListener();
  }

  authListener() {
    fire.auth().onAuthStateChanged((user) => {
      console.log(user);
      if (user) {
        this.setState({ user });
        localStorage.setItem('user', user.uid);
      } else {
        this.setState({ user: null });
        localStorage.removeItem('user');
      }
    });
  }

  render() {
    return (
      <div>
        <Header user={this.state.user}/>
        { this.state.user ? (<div><Search/><MovieList/></div>) : (<Login />)}
        <Footer/>
      </div>
    );
  }
}

export default App;
